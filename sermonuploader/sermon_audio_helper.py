"""Sermon-audio specific ripping and encoding helper methods.

Created on Oct 17, 2012

@author: josh
"""

import audio_tools
import os
import sys
from config import cd_ripping_options
from config import mp3_encoding_options
from config import audio_processing_options

def wavrip(track, dest_file):
    """Rip audio file from cd drive to file of wav format
    
    cd drive - Drive to rip from (0-N)
    track - Track to rip from CD drive (1-N) or 'all'
    dest_file - filename to extract file to
    """
    
    ripper = audio_tools.CDRip(freac_path=cd_ripping_options['freac_directory'])
    ripargs = ["-e", "WAVE",
               "-o", dest_file,
               "-cd", str(cd_ripping_options['source_cddrive']),
               "-t", str(240), # give the extraction 4 minutes before giving up
               "-track",  str(track) 
               ]
    ripper.rip(ripargs)
    if not os.path.exists(dest_file):
        raise IOError("CD Audio extraction did not appear to create an output file: <" + dest_file + ">.")
        
def post_process_audio(source_file, dest_file):
    """Post processes audio for best listening experience using stereo_tool_cmd.exe"""
    
    processor = audio_tools.StereoTool(stereo_tool_path=audio_processing_options['stereo_tool_directory'])
    absolute_sts_file = audio_processing_options['stereo_tool_file']
    
    if not os.path.exists(absolute_sts_file):
        raise IOError("Stereo Tool STS file not found: " + absolute_sts_file)
        
    args = ["--sts", absolute_sts_file,
            "--silent",
            source_file,
            dest_file,
            ]
    processor.process(args)
    if not os.path.exists(dest_file):
        raise IOError("Post processing did not appear to create an output file: <" + dest_file + ">.")

def mp3encode(sermon, source_file, dest_file):
    """Convert source file to destination file
    
    sermon - Object that represents the sermon being encoded
    source_file - Source wave file
    dest_file - Destination mp3 file
    """
    
    encoder = audio_tools.LameEncoder(lame_path=mp3_encoding_options['lame_directory'])
    encargs = list(mp3_encoding_options['lame_options']); # make an explicit copy here so we don't end up modifying the original
    encargs.extend([
      "--tt", sermon.title + " - " + sermon.speaker.first_name + " " + sermon.speaker.last_name, # title
      "--ta", mp3_encoding_options["artist_name"], # artist
      "--tl", mp3_encoding_options["album_name"], #album
      "--ty", str(sermon.date.year), #year
      # "--tn", str(sermon.part), # track number
      "--tg", "Sermon", # genre
      "--tc", sermon.date.isoformat(), # comment
      "--add-id3v2",
      source_file,
      dest_file,
      ])
    encoder.encode(encargs)
    if not os.path.exists(dest_file):
        raise IOError("MP3 encoding did not appear to create an output file: <" + dest_file 
                      + ">. The input file was: <" + source_file + ">.")
        
if __name__ == "__main__":
    pass
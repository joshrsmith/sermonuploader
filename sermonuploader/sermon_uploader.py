"""Main module for ripping, encoding, and uploading sermons.

This makes use of the sermon_database module for managing previously entered data.
This allows for easier/quicker entry of information.
"""
import collections
import os
import sys
import subprocess
import string
import unicodedata
import argparse
from config import cd_ripping_options
from config import audio_processing_options
from config import mp3_encoding_options
from config import general_options
import menuutil
import sermon_audio_helper
import sermon_database
import uploader

_validFilenameChars = "-_.()' %s%s" % (string.ascii_letters, string.digits)

def removeDisallowedFilenameChars(filename):
    cleanedFilename = unicodedata.normalize('NFKD', filename).encode('ASCII', 'ignore')
    return ''.join(c for c in cleanedFilename if c in _validFilenameChars)

def sermon_filename(sermon, file_type):
    """Create a unique name for a sermon from a sermon object"""
    name = "%s. %s - %s. %s" % (sermon.date.isoformat(),
                                sermon.title,
                                sermon.speaker.first_name[0], # first initial
                                sermon.speaker.last_name)
    filename = name + "." + file_type
    return removeDisallowedFilenameChars(unicode(filename))

class SermonUploader():
    """This is the main application class for the uploader application.
    
    It is driven by command line user input.
    """
    
    def __init__(self,db):
        """"Create the sermon uploader.
        
        db - A sermon database for the uploader to interface with
        """
        
        self.db = db

    def rip_and_upload(self):
        """"Select (or enter) a sermon, rip it from CD, and then upload it"""
        sermon = self.db.user_select_sermon()
        self.rip_from_cd(sermon)
        self.upload(sermon)
            
    def rip_from_cd(self, sermon=None):
        """Rip audio track from cd, post process, and then encode it to MP3
        
        If the sermon provided is None, the user will be prompted to select a sermon to upload.
        If a sermon is provided, then a new sermon media file is created based on the sermon information
        """
        
        extract = True
        
        if sermon is None:
            sermon = self.db.user_select_sermon()
            
        full_path_wav_file = os.path.join(general_options['media_directory'],sermon_filename(sermon, 'wav'))
        full_path_wav_file_post_proc = os.path.join(general_options['media_directory'],sermon_filename(sermon, 'wav-pp'))
        full_path_mp3_file = os.path.join(general_options['media_directory'],sermon_filename(sermon, 'mp3'))
        
        try:        
            if os.path.exists(full_path_wav_file):
                print "It appears that sermon audio (wav) already exists..."
                ret = menuutil.menu("Extract again, or skip extraction?", ["Skip extraction", "Re-extract"])
                if ret == 0:
                    extract = False
            
            encoder_input_file = full_path_wav_file # default to the original wav, not post processed one
            if extract:
                print "Please ensure sermon audio CD is now in drive."
                raw_input("Press <ENTER> when CD is ready")
                print "Now extracting audio from CD, please wait..."
                print "Output File: " + full_path_wav_file
                sermon_audio_helper.wavrip(track=1, dest_file=full_path_wav_file)
                print "Audio CD extraction complete"
                print " "
                
                if audio_processing_options['stereo_tool_enable'] is True:
                    # remove the output file, if it happens to already exist
                    if (os.path.exists(full_path_wav_file_post_proc)):
                        os.unlink(full_path_wav_file_post_proc)
                    print "Post processing audio, please wait..."
                    print "Output File: " + full_path_wav_file_post_proc
                    sermon_audio_helper.post_process_audio(source_file=full_path_wav_file,
                                                           dest_file=full_path_wav_file_post_proc)
                    print "Audio Post processing complete"
                    print " "
                    encoder_input_file=full_path_wav_file_post_proc
            
            print "Now encoding / compressing audio, please wait..."
            print "Input File: " + encoder_input_file
            print "Output File: " + full_path_mp3_file
            sermon_audio_helper.mp3encode(sermon, encoder_input_file, full_path_mp3_file)
            print "Audio file encoding complete"
            print " "
            # Delete the original wav file since it is no longer needed at this point and will just take up space
            if (os.path.exists(full_path_wav_file)):
                os.unlink(full_path_wav_file)
            if (os.path.exists(full_path_wav_file_post_proc)):
                os.unlink(full_path_wav_file_post_proc)
        except IOError as e:
            print e
        except subprocess.CalledProcessError as e:
            print "Problem in running. Subprocess returned non-zero exit code. Terminating."
            print e
            sys.exit()
    
    def upload(self, sermon=None):
        """Upload a sermon.
        
        If the sermon supplied is None, the user will be prompted to select a sermon to upload.
        If a sermon is supplied, the media associated with that sermon will be uploaded.
        """
        
        if sermon is None:
            sermon = self.db.user_select_sermon()
        full_path_mp3_file = os.path.join(general_options['media_directory'],sermon_filename(sermon, 'mp3'))
        if not os.path.exists(full_path_mp3_file):
            print "Does not appear that the file has yet been extracted and encoded."
            print "You must extract and encode the sermon first prior to uploading..."
        else:
            try:
                print "Starting upload process..."
                print "Upload File (%s MB): %s" % (os.path.getsize(full_path_mp3_file)/2**20, full_path_mp3_file)
                uploader.upload(sermon, full_path_mp3_file)
                print "Upload complete"
                print " "
            except Exception as e:
                print "Something went wrong with the upload process:"
                print e
                print "Try to upload again later."            
    
    def list_sermons(self):
        """Print out the information for all sermons in the database"""
        
        print "Sermons in the database:"
        sermons = self.db.get_sermons(return_all=True)
        for sermon in sermons:
            sermon.print_info()
        print "End of list"
    
    def list_speakers(self):
        """Print out the information for all speakers in the database"""
        
        print "Speakers in the database:"
        speakers = self.db.get_speakers(return_all=True)
        for speaker in speakers:
            speaker.print_info()
        print "End of list"
        
    def run(self):
        """Run one iteration of the sermon_uploader.
        For each run, the user can select one control path from the menu, which will be executed
        """
        
        menu = collections.OrderedDict()
        menu["Rip Sermon from CD and Upload"] = self.rip_and_upload
        menu["Rip Sermon from CD"] = self.rip_from_cd
        menu["Upload sermon"] = self.upload
        menu["List Sermons in Database"] = self.list_sermons
        menu["List Speakers in Database"] = self.list_speakers
        menu["Finished / Exit"] = sys.exit
        
        print "===================================="
        print "Welcome to the Sermon Audio Uploader"
        print "===================================="
        do = menuutil.objectmenu("Please choose from following options:", menu)
        do()
        
if __name__ == "__main__":
    # This is the main entry point of execution for the entire program
    
    parser = argparse.ArgumentParser(description='Rip, Encode, and Upload Sermons')
    parser.add_argument('--init', action='store_true', help='initialize the database storing sermon information')
    parser.add_argument('--version', action='version', version='%(prog)s 1.2')
    args = parser.parse_args()
    initialize = args.init
    
    if initialize and os.path.exists(general_options['database_file']):
        print "You are about to initialize the sermon database: " +  general_options['database_file']
        print "This will eliminate all current database entries and re-create an empty database."
        if menuutil.menu("Re-create an empty database?", ["No", "Yes"]) == 1:
            initialize = True
        else:
            initialize = False
            print "Continuing without making changes to the database."
    db = sermon_database.SermonDatabase(database_file=general_options['database_file'], initialize=initialize)
    su = SermonUploader(db)
    while True:
        run = su.run()
        print " "
        j = raw_input("Press <ENTER> to continue...")
        print " "
        print " "
    
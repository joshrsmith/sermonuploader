"""Methods and classes related to the sermon database. There is a class
to represent each of the tables in the database: Speaker, and Sermon. The database
is only dealt with inside this module. All outside entities deal with objects of the classes
defined here.

Created on Oct 17, 2012

@author: josh
"""

import sqlite3
import os
import datetime
import menuutil
import collections
import exceptions

_NOTFOUND = object()

class Speaker():
    def __init__(self, id, first_name, last_name):
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
    def __eq__(self, other):
        for attr in ['id', 'first_name', 'last_name']:
            v1, v2 = [getattr(obj, attr, _NOTFOUND) for obj in [self, other]]
            if v1 is _NOTFOUND or v2 is _NOTFOUND:
                return False
            elif v1 != v2:
                return False
        return True
    def print_info(self):
        print "%d: First Name=%s, Last Name=%s" % (self.id, self.first_name, self.last_name)
        
class Sermon():
    def __init__(self, id, date, title, speaker):
        self.id = id
        self.date = date
        self.title = title
        self.speaker = speaker
        
    def print_info(self):
        print "%d: Date=%s, Title=%s, Speaker=%s %s" % (self.id, self.date, self.title, self.speaker.first_name, self.speaker.last_name)
        
    def __eq__(self, other):
        for attr in ['id', 'date', 'title']:
            v1, v2 = [getattr(obj, attr, _NOTFOUND) for obj in [self, other]]
            if v1 is _NOTFOUND or v2 is _NOTFOUND:
                return False
            elif v1 != v2:
                return False
        return True

def ask_speaker(stored_speakers):
    speakers = collections.OrderedDict()
    speakers["New Speaker"] = Speaker(id=None, first_name=None, last_name=None) # blank speaker
    for speaker in stored_speakers:
        speakers[speaker.first_name + " " + speaker.last_name] = speaker
    return menuutil.objectmenu("Select a speaker", speakers)

def ask_sermon(stored_sermons):
    sermons = collections.OrderedDict()
    sermons["New Sermon"] = Sermon(id=None, date=None, title=None, speaker=None)
    back = min(len(stored_sermons), 3) # list most recent 3 sermons
    for i in range (-back, 0):
        sermon = stored_sermons[i]
        sermons[sermon.date.isoformat() + ": " + sermon.speaker.first_name + " " + sermon.speaker.last_name + " - " + sermon.title] = sermon
    return menuutil.objectmenu("Select a sermon", sermons)

def SpeakerByInput(s):
    print "Enter the new speaker information..."
    s.first_name = raw_input("First Name: ")
    s.last_name = raw_input("Last Name: ")
    return s

def SermonByInput(s):
    print "Creating sermon by keyboard..."
    title = ""
    while len(title) == 0:
        title = raw_input("Enter title/topic: ")
    s.title = title
    s.date = menuutil.date_menu("Enter Date of Sermon...")
    return s

class SermonDatabase(object):
    
    def __init__(self, database_file = 'sermons.db', initialize=False):
        # If we were asked to initialize the database,
        # then we should remove the one that (may) already exist.
        if initialize:
            if os.path.exists(database_file):
                os.remove(database_file)
        
        # if the database file does not exist, we need to initialize it
        if not os.path.exists(database_file):
            initialize = True
            
        self.db = sqlite3.connect(database_file)
        self.c = self.db.cursor()
        
        
        
        if initialize:
            self.setup_database()
            
        self.verify_database_compatibility(1)
    
    def setup_database(self):
        self.create_tables()
        self.insert_this_version()
        self.db.commit()
        
    def create_tables(self):
        self.c.execute('''CREATE TABLE speaker
                        (ID INTEGER PRIMARY KEY ASC,
                        FIRST_NAME TEXT NOT NULL,
                        LAST_NAME TEXT NOT NULL
                        )''')
        self.c.execute('''CREATE TABLE sermon
                        (ID INTEGER PRIMARY KEY ASC,
                        DATE TEXT NOT NULL,
                        TITLE TEXT NOT NULL,
                        SPEAKERID INTEGER REFERENCES speaker(ID)
                        )''')
        self.c.execute('''CREATE TABLE version_history
                        (VERSION INTEGER PRIMARY KEY ASC,
                        UPGRADE_DATETIME TEXT
                        )''')
    def insert_this_version(self):
        self.c.execute("INSERT INTO version_history VALUES (NULL,?)", (datetime.datetime.now(),))
    def get_database_version(self):
        self.c.execute("SELECT version FROM version_history")
        versions = self.c.fetchall()
        return versions[-1][0]
    def verify_database_compatibility(self, current_program_version):
        version = self.get_database_version()
        if version < current_program_version:
            raise Exception("Sermon Database must be upgraded.")
        elif version > current_program_version:
            raise Exception("Program is outdated and not compatible with the database")
    def __value_in_column(self, table, column, value):
        ''' Checks if <value> is present in the column <column> in table <table> '''
        self.c.execute("SELECT * FROM %s WHERE %s = '%s'" % (table, column, value))
        val = self.c.fetchone()
        if val == None:
            incolumn = False
        else:
            incolumn = True
        return incolumn
        
    def update_speaker(self, speaker):
        ''' Add or update a speaker in the database 
        Note that this function updates the id of the speaker
        that was passed in. It will also return the modified object'''
        if speaker.id == None: # new to the database
            self.c.execute("INSERT INTO speaker VALUES (NULL,?,?)", (speaker.first_name, speaker.last_name))
            rowid = self.c.lastrowid
        else: # update the database
            raise exceptions.NotImplementedError
            self.c.execute("UPDATE speaker SET first_name = '%s', last_name = '%s' WHERE id = %s" % (speaker.first_name, speaker.last_name, speaker.id))
            rowid = speaker.id
        speaker.id = int(rowid)
        return speaker
    
    def update_sermon(self, sermon):
        ''' Add or update a sermon in the database 
        Note that this function updates the id of the sermon
        that was passed in. It will also return the modified object '''
        if sermon.speaker.id is None: # if the speaker does not yet have an id
            sermon.speaker = self.update_speaker(sermon.speaker)
        if sermon.id == None: # new to the database
            self.c.execute("INSERT INTO sermon VALUES (NULL,?,?,?)", (sermon.date, sermon.title, sermon.speaker.id))
            rowid = self.c.lastrowid
        else:
            # TODO Add method for updating the database instead of just inserting
            raise exceptions.NotImplementedError
        sermon.id = int(rowid)
        return sermon
        
    def __get_table(self, table_name, id_list, return_all):
        ''' Assumes a valid list of ids. Empty list is ok. None may not work. '''
        if return_all:
            self.c.execute('SELECT * FROM %s' % table_name)
            rtn = self.c.fetchall()
        else:
            rtn = []
            for idi in id_list:
                self.c.execute("SELECT * FROM %s WHERE ID = %s " % (table_name, idi))
                rtn.append(self.c.fetchone())  
        return rtn

    def get_speakers(self, speakerid_list=[], return_all=False):
        ''' Return a list of speaker objects (or Nones), one item for
        every id requested. If return_all is True, then just return all of
        the speakers and ignore the id list '''
        
        speakers = self.__get_table('speaker', speakerid_list, return_all)
        # Ok, we got a list back, which may or may not have database entries in it. If there were "no match" entries (None), then we will pass
        # the None along. Otherwise, construct a speaker object.
        speakers_list = [Speaker(id=speaker[0], first_name=speaker[1], last_name=speaker[2]) if speaker is not None else None for speaker in speakers] 
        return speakers_list
    
    def get_sermons(self, sermonid_list=[], return_all=False):
        ''' Return a list of sermon objects (or Nones), one item for
        every id requested. If return_all is True, then just return all of
        the sermons and ignore the id list '''
        
        sermons = self.__get_table('sermon', sermonid_list, return_all)
        sermon_list = []
        for sermon in sermons:
            if sermon is None:
                sermon_list.append(None)
            else: # a valid sermon entry was returned
                sermon_list.append(Sermon(id=sermon[0], 
                                          date=datetime.datetime.strptime(sermon[1], "%Y-%m-%d").date(),
                                          title=sermon[2],
                                          speaker=self.get_speakers([sermon[3],])[0]))        
        return sermon_list
    
    def commit(self):
        ''' Make all changes to the database permanent '''
        self.db.commit()
    
    def revert(self):
        ''' Undo all database changes since the last commit '''
        self.db.rollback()
        
    def user_select_sermon(self):
        ''' Uses text based prompting for the user to select or create
        a new sermon in the database. '''
        done = False
        while not done:
            sermon = ask_sermon(self.get_sermons(return_all=True))
            if sermon.id == None: # New sermon
                SermonByInput(sermon)                
                speaker = ask_speaker(self.get_speakers(return_all=True))
                if speaker.id == None: # new speaker
                    SpeakerByInput(speaker)
                    self.update_speaker(speaker)
                sermon.speaker = speaker
                self.update_sermon(sermon)
                print " "
                print " "
                sermon.print_info()
                a = menuutil.menu("Please confirm the above entered sermon information" , ["Confirm", "Start Over"])
                if a == 0:
                    self.commit()
                    done = True
                else:
                    self.revert()
                    done = False
            else:
                done = True
        return sermon
        
if __name__ == "__main__":
    pass
"""Methods to upload sermons to various places.

Created on Nov 3, 2012

@author: josh
"""

import ftplib
import string
from datetime import datetime
from dateutil import tz
from urlparse import urljoin
from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.compat import xmlrpc_client
from wordpress_xmlrpc.methods import media
from wordpress_xmlrpc.methods.posts import NewPost
from config import general_options
from config import wordpress_upload_options
from config import ftp_upload_options
import os

def upload(sermon, filename):

    url = None
    if general_options['use_ftp_upload'] is True:
        url = ftp_upload(filename)
        
    # Create the wordpress interface if we are going to need it
    if general_options['use_wordpress_upload'] or general_options['create_wordpress_post']:
        wp = WordPressInterface(url=wordpress_upload_options['xmlrpc_url'],
                        username=wordpress_upload_options['username'],
                        password=wordpress_upload_options['password'])
        
    if general_options['use_wordpress_upload'] is True:
        url = wp.upload_file(sermon, filename)
    
    if general_options['create_wordpress_post'] is True:
        if url is None:
            raise Exception("URL for uploaded file was None. You can't create a WordPress post without uploading.")
        wp.new_sermon_post(sermon, url)

def ftp_upload(filename):
    """Upload the file to an FTP server
    
    Returns the url to the uploaded file
    """
    
    ftp = ftplib.FTP(host=ftp_upload_options['ip'],
                     user=ftp_upload_options['username'],
                     passwd=ftp_upload_options['password'])
    ftp.cwd(ftp_upload_options['remote_directory'])
    ftp_filename = str(os.path.basename(filename))
    ftp_filename = string.replace(ftp_filename,' ','-')
    ftp.storbinary('STOR %s' % ftp_filename, open(filename,'rb'))
    ftp.close()
    url = urljoin(ftp_upload_options['website_url'], ftp_filename)
    return url

class WordPressInterface:
    def __init__(self, url, username, password, blog_id=0):  
        self. client = Client(url=url, username=username, password=password, blog_id=blog_id)
    
    def new_sermon_post(self, sermon, media_url):
        ''' Create a wordpress sermon post with reference to a media url '''
        post = WordPressPost()
        post.title = "%s, %s" % (sermon.title, sermon.date.isoformat())
        post.content = "This week's sermon is by %s %s.<br/>[audio mp3=\"%s\"]" % (sermon.speaker.first_name, sermon.speaker.last_name, media_url)
        post.content += "<br/><a href=\"%s\">[Right Click > Save As... to download]</a>" % (media_url) # add a link to directly download instead of play
        
        # Use the sermon's date to modify the WordPress post date
        # We are going to use the UTC time, and the date of the sermon together.
        utctz = tz.tzutc()
        now_time = datetime.now(utctz).time()
        post_datetime = datetime(year=sermon.date.year, month=sermon.date.month, day=sermon.date.day)
        post_datetime = post_datetime.replace(hour=now_time.hour,
                                              minute=now_time.minute,
                                              second=now_time.second,
                                              tzinfo=utctz)
        post.date = post_datetime
        
        post.terms_names = {}
        if wordpress_upload_options['tag_posts'] is True:
            post.terms_names['post_tag'] = ["%s %s" % (sermon.speaker.first_name, sermon.speaker.last_name),]
        
        post.terms_names['category'] = wordpress_upload_options['post_category_list']
        
        if wordpress_upload_options['publish'] == True:
            post.post_status = 'publish'
        else:
            post.post_status = 'draft'
        self.client.call(NewPost(post))
        
    def upload_file(self, sermon, filename):
        ''' Upload a file to a wordpress website.
        Returns the url of the file on the site '''
        
        basefile = os.path.basename(filename)
        # prepare metadata
        data = {
            'name': basefile,
            'type': 'audio/mpeg',  # mimetype
        }
        
        # read the binary file and let the XMLRPC library encode it into base64
        with open(filename, 'rb') as img:
            data['bits'] = xmlrpc_client.Binary(img.read())
        
        response = self.client.call(media.UploadFile(data))
        
        # Example response
        # response == {
        #       'id': 6,
        #       'file': 'picture.jpg'
        #       'url': 'http://www.example.com/wp-content/uploads/2012/04/16/picture.jpg',
        #       'type': 'image/jpg',
        # }

        return response['url']    
    
    

    

import yaml
import sys
import os

__thismodule = sys.modules[__name__]

# This was added for compatibility with PyInstaller
if getattr(sys, 'frozen', None):
    __basedir = sys._MEIPASS
else:
    __basedir = os.path.dirname(__file__)

__location = os.path.realpath(
    os.path.join(os.getcwd(), __basedir))

__config_file = os.path.join(__location, 'config.yml')

try:
    with file(__config_file,'r') as f:
        __doc = yaml.load(f)
        for key, value in __doc.iteritems():
            setattr(__thismodule, key, value)
    
    if getattr(__thismodule, 'customized') == False:
        raise Exception("You must customize " + __config_file + " prior to using this program")
    
except IOError:
    raise IOError(__config_file + " could not be opened. Ensure that config.yml was updated")
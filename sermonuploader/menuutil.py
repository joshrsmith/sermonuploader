"""A collection of utilities for gathering user input from textual selection menus

Created on Oct 18, 2012

@author: josh
"""

import datetime
import calendar

def objectmenu(query_string, items):
    """A command-line driven menu which returns the value of selected key-value pair
    
    query_string - The menu style question
    items - A dictionary of key value pairs.
    """
    
    continue_test = True
    keys = items.keys()
    while continue_test:
        print query_string
        for keyi in range(len(keys)):
            print str(keyi+1) + ") " + keys[keyi] 
        try:
            var = int(raw_input("Enter selection value: "))
            if var >= 1 and var <= len(items):
                continue_test = False
        except ValueError:
            print "Not an integer value. Please enter integer value"
            continue_test = True
    return items[keys[var-1]]

def menu(query_string, items):
    """A basic menu which returns the numeric value of the selected item"""
    continue_test = True
    while continue_test:
        print query_string
        for i in range(len(items)):
            print str(i+1) + ") " + str(items[i])
        try:
            var = int(raw_input("Enter selection value: "))
            if var >= 1 and var <= len(items):
                continue_test = False
        except ValueError:
            print "Not an integer value. Please enter integer value"
            continue_test = True
    return (var-1)

def prompt_int_value(query_string, min_value, max_value):
    """Prompt for an integer value within a specified range"""
    continue_test = True
    while continue_test:
        try:
            var = int(raw_input(query_string + " (%s-%s): " % (str(min_value), str(max_value))))
            if var >= min_value and var <= max_value:
                continue_test = False
        except ValueError:
            print "Not an integer value. Please enter integer value"
            continue_test = True
    return var

def date_menu(query_string):
    """Pick a date"""
    a = menu(query_string, ["Today", "Other Date"])
    if a == 0:
        date = datetime.date.today()
    elif a == 1:
        today = datetime.date.today()
        # prompt year
        year = prompt_int_value("Enter Year", today.year-2, today.year)
        # prompt month
        if year == today.year:
            max_month = today.month
        else:
            max_month = 12
        month = prompt_int_value("Enter Month", 1, max_month)
        
        # prompt day
        dayrange = calendar.monthrange(year,month)
        if year == today.year and month == today.month:
            max_day = today.day
        else:
            max_day = dayrange[1]
        day = prompt_int_value("Enter Day", 1, max_day)
        
        date = datetime.date(year, month,day)
        
    return date

if __name__ == "__main__":
    date = date_menu("Select Date of Sermon")
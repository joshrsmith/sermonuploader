#!/usr/bin/env python

from distutils.core import setup

setup(
   name = "sermonuploader",
   version = "1.3",
   description = "Extract recorded sermon from CD, encode, tag and upload",
   author = "Josh Smith",
   author_email = "joshrsmith@gmail.com",
   maintainer = "Josh Smith",
   maintainer_email = "joshrsmith@gmail.com",
   license = "MIT",
   url = "https://bitbucket.org/joshrsmith/sermonuploader/",
   packages = ['sermonuploader',],
   package_data={'sermonuploader': ['config.yml']},
   platforms = ['Windows'],
#   requires=[
#          'python-wordpress-xmlrpc',
#          'pyyaml'
#          ],
   install_requires=[
          'python-wordpress-xmlrpc',
          'pyyaml',
          'python-dateutil',
          ],
   scripts=[
            'bin/sermonuploader.bat',
            ]
      )
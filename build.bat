rem First argument is the version we are building
IF [%1]==[] exit /b 1

set pyenv=C:\pyenv\sermonuploader\Scripts
set pyinstall=C:\pyscript\pyinstaller-2.0

call %pyenv%\activate

rem clean up
rd /S /Q dist
rd /S /Q build

rem Build the executable
call python "%pyinstall%\pyinstaller.py" "sermon_uploader.spec"
move "dist\sermon_uploader" "dist\sermonuploader_exe-%1"

rem Zip it up
cd dist
call 7za a "sermonuploader_exe-%1.zip" "sermonuploader_exe-%1"
cd ..

rem clean up build
rd /S /Q build
rd /S /Q "dist\sermonuploader_exe-%1"

rem Build the source distribution
call python setup.py sdist

copy README dist\
copy LICENSE dist\

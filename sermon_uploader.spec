# -*- mode: python -*-
a = Analysis(['sermonuploader\\sermon_uploader.py'],
             pathex=['sermonuploader'],
             hiddenimports=[],
             hookspath=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=1,
          name=os.path.join('build\\pyi.win32\\sermon_uploader', 'sermon_uploader.exe'),
          debug=False,
          strip=None,
          upx=True,
          console=True )
          
additional_files_toc = [('config.yml','sermonuploader\\config.yml','DATA'),
 						('README','README','DATA'),
 						('LICENSE','LICENSE','DATA'),]          
coll = COLLECT(exe,
               a.binaries,
               additional_files_toc,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name=os.path.join('dist', 'sermon_uploader'))
